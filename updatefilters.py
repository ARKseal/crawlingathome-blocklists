import os
import shutil
from datetime import datetime

shutil.rmtree('blocklists', ignore_errors=True)
os.system('rsync -zh archiveteam@88.198.2.17::bloom/*.bin blocklists')
os.system('git add .')
os.system(f'git commit -m "update blocklists {datetime.now():"%m/%d/%Y"}"')
os.system(f'git push')